import json
import os
import sys
packages_path = os.path.join(os.path.split(__file__)[0], "packages")
sys.path.append(packages_path)
import requests

def lambda_handler(event, context):
    # TODO implement
    query = {
        "target": {
        "type": "pipeline_ref_target",
        "ref_type": "branch",
        "ref_name": "master",
        "selector": {
            "type": "custom",
            "pattern": "staging"
        }
        }
    }
    my_headers = {
        "Content-Type": "application/json"
    }
    response = requests.post("https://api.bitbucket.org/2.0/repositories/vishnu1106/qacustompipelinelambda/pipelines/", auth=HTTPBasicAuth('vishnu1106', 'Switch@0032'), params=query, headers = my_headers)
    print(response)
    return {
        'statusCode': 200,
        'body': json.dumps('Hello from Lambda!')
    }
