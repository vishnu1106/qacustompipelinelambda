import json
import boto3

#testing invocation
client = boto3.client('lambda', region_name='us-east-2')
def lambda_handler(a,b):
    inputParams = {
        "Product Name":"Iphone 13",
        "Quantity" : 10,
        "Unit Price":1024
    }
    response = client.invoke(
        FunctionName = "arn:aws:lambda:us-east-2:460413214814:function:child-lambda-function",
        InvocationType ="RequestResponse",
        Payload = json.dumps(inputParams)
    )
    print(response)
    responseCame = json.load(response['Payload'])
    print('\n')
    print(responseCame)
    return "Lambda function called"